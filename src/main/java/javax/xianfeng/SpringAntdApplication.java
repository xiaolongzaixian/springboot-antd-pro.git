package javax.xianfeng;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @since 2020/01/03 09:21:22
 */
@EntityScan(basePackages = "com.hello123")
@EnableJpaRepositories(basePackages = "com.hello123")
@ComponentScan(basePackages = { "javax.xianfeng", "com.hello123" })
@ServletComponentScan(basePackages = { "javax.xianfeng", "com.hello123" })
@SpringBootApplication
public class SpringAntdApplication {

	/**
	 * @param args
	 * @since 2020/01/03 09:21:22
	 */
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(SpringAntdApplication.class);
		application.setBannerMode(Banner.Mode.OFF);
		application.run(args);
	}

}
