package com.hello123.service.demo;

import java.sql.Date;
import java.util.List;

import javax.xianfeng.antdpro.model.ApiResult;
import javax.xianfeng.antdpro.model.PagingApiResult;

import com.hello123.model.demo.User;

/**
 * @since 2020/01/28 12:40:25
 */
public interface UserService {

	public ApiResult get(Long id);

	public PagingApiResult list(Integer pageSize, Integer pageNum, String sortField, String sortDir, String type,
			String name, Byte sex, Date birthday, Integer age);

	public void save(User user);

	public void remove(List<Long> ids);

	public void update(User user);

	void batchDelete(List<Long> ids);

	void deleteByIdIn(List<Long> ids);
}
