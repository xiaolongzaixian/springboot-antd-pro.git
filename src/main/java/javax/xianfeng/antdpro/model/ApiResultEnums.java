package javax.xianfeng.antdpro.model;

/**
 * @since 2020/01/28 15:20:16
 */
public enum ApiResultEnums {

	SUCCESS(1000),
	
	FAILURE(-1000);
	
	private int code;
	
	ApiResultEnums(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return this.code;
	}
	
	public String getText() {
		return this.toString();
	}
}
