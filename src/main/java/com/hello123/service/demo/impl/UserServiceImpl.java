package com.hello123.service.demo.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.xianfeng.antdpro.model.ApiResult;
import javax.xianfeng.antdpro.model.Paging;
import javax.xianfeng.antdpro.model.PagingApiResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.hello123.dao.demo.UserDao;
import com.hello123.model.demo.User;
import com.hello123.service.demo.UserService;

/**
 * @since 2020/01/28 12:52:25
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public ApiResult get(Long id) {
		User user = userDao.getById(id);
		return new ApiResult(user);
	}

	@Override
	public PagingApiResult list(Integer pageSize, Integer pageNum, String sortField, String sortDir, String type,
			String name, Byte sex, Date birthday, Integer age) {
		if (Objects.isNull(pageSize)) {
			pageSize = 10;
		}

		if (Objects.isNull(pageNum)) {
			pageNum = 1;
		}

		// 默认排序字段为id
		if (StringUtils.isEmpty(sortField)) {
			sortField = "id";
		}

		// 默认排序为升序
		Sort.Direction direction = Sort.Direction.ASC;
		if (Sort.Direction.DESC.toString().equalsIgnoreCase(sortDir)) {
			direction = Sort.Direction.DESC;
		}
		Sort sort = new Sort(direction, sortField);

		Specification specification = new Specification() {
			@Override
			public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicateList = new ArrayList<>();

				if (!StringUtils.isEmpty(type)) {
					predicateList.add(criteriaBuilder.equal(root.get("type"), type));
				}

				if (!StringUtils.isEmpty(name)) {
					predicateList.add(criteriaBuilder.like(root.get("name"), "%" + name + "%"));
				}

				if (!StringUtils.isEmpty(sex)) {
					predicateList.add(criteriaBuilder.equal(root.get("sex"), sex));
				}

				if (!StringUtils.isEmpty(birthday)) {
					predicateList.add(criteriaBuilder.equal(root.get("birthday"), birthday));
				}

				if (!StringUtils.isEmpty(age)) {
					predicateList.add(criteriaBuilder.equal(root.get("age"), age));
				}

				return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};

		PageRequest pageRequest = PageRequest.of(pageNum - 1, pageSize, sort);
		Page<User> pageResult = userDao.findAll(specification, pageRequest);

//		Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);
//		Page<User> pageResult = userDao.findAll(pageable);

		PagingApiResult result = new PagingApiResult();
		result.setList(pageResult.getContent());
		Paging paging = new Paging();
		paging.setTotal(pageResult.getTotalElements());
		paging.setCurrent(pageNum);
		paging.setPageSize(pageSize);
		result.setPagination(paging);
		return result;
	}

	@Override
	public void save(User user) {
		if (StringUtils.isEmpty(user.getType())) {
			user.setType("user");
		}
		userDao.save(user);
	}

	@Override
	public void remove(List<Long> ids) {
		userDao.batchDelete(ids);
	}

	@Override
	public void update(User user) {
		userDao.update(user.getId(), user.getType(), user.getName(), user.getSex(), user.getBirthday(), user.getAge(),
				user.getDesc());

	}

	@Override
	public void batchDelete(List<Long> ids) {
		userDao.batchDelete(ids);

	}

	@Override
	public void deleteByIdIn(List<Long> ids) {
		userDao.deleteByIdIn(ids);
	}

}
